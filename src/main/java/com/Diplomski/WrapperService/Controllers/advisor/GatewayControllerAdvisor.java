package com.Diplomski.WrapperService.Controllers.advisor;

import com.Diplomski.WrapperService.Controllers.models.response.BaseResponse;
import com.Diplomski.WrapperService.Handlers.Exceptions.BadRequestException;
import com.Diplomski.WrapperService.Handlers.Exceptions.NoMainClassFoundException;
import com.Diplomski.WrapperService.Handlers.Exceptions.base.HandledException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice(basePackages = {"com.Diplomski.WrapperService.Controllers"})
public class GatewayControllerAdvisor {

    @ExceptionHandler(value = {HandledException.class})
    public ResponseEntity<BaseResponse> handleHandledException(HandledException exception, WebRequest req) {
        final HttpStatus httpStatus = exception.getHttpStatus();
        final String errorMessage = exception.getMessage();
        final BaseResponse baseResponse = new BaseResponse(httpStatus.value(), httpStatus.getReasonPhrase(), errorMessage);

        return new ResponseEntity<>(baseResponse, httpStatus);
    }
}
