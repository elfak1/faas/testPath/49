package com.Diplomski.WrapperService.Handlers.Contracts;

import java.util.List;

public interface IHandler {

    Object execute(List<Object> parameters);

    String getUserCode();
}