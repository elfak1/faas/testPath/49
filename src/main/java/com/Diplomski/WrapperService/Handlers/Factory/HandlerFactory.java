package com.Diplomski.WrapperService.Handlers.Factory;

import static com.Diplomski.WrapperService.Handlers.Contracts.HandlerTypes.JAVA;

import com.Diplomski.WrapperService.Handlers.Contracts.HandlerTypes;
import com.Diplomski.WrapperService.Handlers.Contracts.IHandler;
import com.Diplomski.WrapperService.Handlers.Contracts.IHandlerFactory;
import com.Diplomski.WrapperService.Handlers.Exceptions.HandlerNotFoundException;
import com.Diplomski.WrapperService.Handlers.JavaHandler;
import java.util.HashMap;
import java.util.Map;

public class HandlerFactory implements IHandlerFactory {

    private final Map<HandlerTypes, IHandler> handlerTypesIHandlerMap;


    public HandlerFactory(final String userCodePath) {
        handlerTypesIHandlerMap = new HashMap<>();
        handlerTypesIHandlerMap.put(JAVA, new JavaHandler(userCodePath));
    }

    @Override
    public IHandler getHandler(HandlerTypes handlerType) throws HandlerNotFoundException {
        if (!handlerTypesIHandlerMap.containsKey(handlerType)) {
            throw new HandlerNotFoundException(handlerType);
        }
        
        return handlerTypesIHandlerMap.get(handlerType);
    }
}
