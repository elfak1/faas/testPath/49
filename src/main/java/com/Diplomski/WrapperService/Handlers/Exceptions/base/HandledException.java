package com.Diplomski.WrapperService.Handlers.Exceptions.base;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class HandledException extends RuntimeException {

    private final HttpStatus httpStatus;

    public HandledException() {
        super();
        httpStatus = null;
    }

    public HandledException(final String message, final HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }
}
