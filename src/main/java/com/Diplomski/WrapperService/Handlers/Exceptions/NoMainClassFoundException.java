package com.Diplomski.WrapperService.Handlers.Exceptions;

import com.Diplomski.WrapperService.Handlers.Exceptions.base.HandledException;
import org.springframework.http.HttpStatus;

public class NoMainClassFoundException extends HandledException {

    private static final String errorMessage = "No main class found in user code.";

    public NoMainClassFoundException() {
        super(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
