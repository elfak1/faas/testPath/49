package com.Diplomski.WrapperService.Handlers.Exceptions;

import com.Diplomski.WrapperService.Handlers.Contracts.HandlerTypes;

public class HandlerNotFoundException extends RuntimeException {

    public HandlerNotFoundException(final HandlerTypes handlerType) {
        super("No handler was found for the given language, language was: " + handlerType);
    }

}
