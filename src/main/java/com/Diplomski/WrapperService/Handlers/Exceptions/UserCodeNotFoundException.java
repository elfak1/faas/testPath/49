package com.Diplomski.WrapperService.Handlers.Exceptions;

public class UserCodeNotFoundException extends RuntimeException {

    public UserCodeNotFoundException(Exception exception) {
        super(exception);
    }
}
